package Driver;

/****************************
 * Stream Sounds            *
 * @author Stefan Lovric    *
 * Date started: 08/03/2019 *
 * Date fnished: 8/25/2019  *
 ****************************/

import Controller.Home;
import Model.User;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import java.io.IOException;

/**
 * Driver for application -- handles initialization and exit
 */
public class StreamSounds extends Application {

    /** Pointer to the active user singleton */
    public static User user;

    /** Pointer to the primary stage the javafx application is running on */
    public static Stage primaryStage;

    /**
     * Execution context starts here
     * @param args Arguments to the program (of which none will be parsed)
     */
    public static void main(String[] args) {

        /* Load user data into a User instance (deserialization) */
        user = User.loadUserData();

        /* Startup the application thread */
        launch(args);

        /* Serialize user's session */
        User.saveUserData();
    }

    /**
     * Initializes the application
     * @param primaryStage Main stage the application will launch on
     */
    @Override
    public void start(Stage primaryStage) throws IOException {
        this.primaryStage = primaryStage;
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/View/home.fxml"));
        AnchorPane root = loader.load();
        primaryStage.setMinHeight(root.getPrefHeight());
        primaryStage.setMinWidth(root.getPrefWidth());
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.getIcons().add(new Image(getClass().getResource("/Icons/eighth-notes.png").toExternalForm()));
        primaryStage.setTitle("Stream Sounds");
        primaryStage.show();
        Home home = loader.getController();
        home.start();
    }
}
