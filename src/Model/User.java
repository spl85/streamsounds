package Model;

import java.io.*;
import java.util.ArrayList;

/**
 * Class representation of a User -- A user aggregates playlists and songs (singleton)
 */
public class User implements Serializable {

    /** Serial UID */
    static final long serialVersionUID = 42L;

    /** User's playlists */
    private ArrayList<Playlist> playlists;

    /** User's song library */
    private ArrayList<Song> songLibrary;

    /** Singleton pointer */
    public static User user;

    /** User data file path */
    public static String userStash;

    /** Audio data file path */
    public static String audioDir;

    /** Image data file path */
    public static String imageDir;

    /**
     * Constructs a User object with default null values
     */
    private User(){
        this.playlists = new ArrayList<>();
        this.songLibrary = new ArrayList<>();
    }

    /**
     * Constructs a User object with predetermined values
     * @param p List of playlists for the user
     * @param s List of songs for the user
     */
    private User(ArrayList<Playlist> p, ArrayList<Song> s){
        this.playlists = p;
        this.songLibrary = s;
    }

    /**
     * Constructs a new User object if and only if there are no current instances of the object being referenced and deserializes user data
     * @return A singleton User instance populated with values acquired from deserialization if there are currently no other instances of this class,
     * otherwise returns null
     */
    public static User loadUserData() {
        if(user != null)
            return null;
        File file = new File("");
        File userSessionDir = new File(file.getAbsolutePath().concat(File.separator + "Resources"  + File.separator + "User_Data"));
        if(!userSessionDir.exists())
            userSessionDir.mkdirs();
        File userSessionFile = new File(userSessionDir.getAbsolutePath().concat(File.separator + "user_session.dat"));
        audioDir = userSessionDir.getAbsolutePath().concat(File.separator + "Audio");
        File audioDir = new File(User.audioDir);
        if(!audioDir.exists())
            audioDir.mkdir();
        imageDir = userSessionDir.getAbsolutePath().concat(File.separator + "Images");
        File imageDir = new File(User.imageDir);
        if(!imageDir.exists())
            imageDir.mkdir();
        userStash = userSessionFile.getAbsolutePath();
        if(!userSessionFile.exists()) {
            try {
                userSessionFile.createNewFile();
            } catch (IOException e) {
            }
        }
        ObjectInputStream ois;
        try{
            ois = new ObjectInputStream(new FileInputStream(userSessionFile));
            user = (User)ois.readObject();
            ois.close();
        } catch(EOFException e){
            user = new User();
            return user;
        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        } catch (ClassNotFoundException e) {
        }
        return user;
    }

    /**
     * Serializes user data into appropriate locations (images go into Images dir, audio files go into Audio dir,
     * and all other data that is not as byte heavy (primitives and strings) into user_session.dat
     */
    public static void saveUserData(){
        ObjectOutputStream oos;
        try{
            oos = new ObjectOutputStream(new FileOutputStream(new File(User.userStash)));
            oos.writeObject(User.user);
            oos.close();
        } catch(FileNotFoundException e){
        } catch(IOException e){
        }
    }

    /**
     * Gets a file extension from a file
     * Ex: 'mp3'
     * @param file File to pull extension from
     * @return String containing the file's extension
     */
    public static String getFileExtension(File file){
        String fileName = file.getName();
        int i = 0;
        String extension = "";
        while(i < fileName.length()){
            if(fileName.charAt(i) == '.' && i != fileName.length()){
                extension = "";
            } else{
                extension += fileName.charAt(i);
            }
            ++i;
        }
        return extension;
    }

    /**
     * Gets the user's current playlists
     * @return A List of the user's current playlists
     */
    public ArrayList<Playlist> getPlaylists(){
        return playlists;
    }

    /**
     * Gets the user's current song library
     * @return A List of all the songs in the user's current library
     */
    public ArrayList<Song> getSongLibrary(){
        return songLibrary;
    }
}
