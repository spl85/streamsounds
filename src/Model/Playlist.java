package Model;

import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import java.io.*;
import java.util.ArrayList;

/**
 * Class representation of a playlist -- acts as a collection of songs
 */
public class Playlist implements Serializable {

    /** Serial UID */
    private static final long serialVersionUID = 43L;

    /** Image of the playlist */
    private transient Image image;

    private File imageFile;

    /** Name of the playlist */
    private String name;

    /** List of songs in the playlist */
    private ArrayList<Model.Song> songs;

    /** Constructs a new Playlist object with default values*/
    public Playlist(){
        this.image = null;
        this.name = null;
        this.songs = new ArrayList<>();
    }

    /**
     * Constructs a new Playlist object with parameter values
     * @param image Image of the playlist
     * @param imageFile Image file of the playlist
     * @param name Name of the playlist
     * @param songs Songs in the playlist
     */
    public Playlist(Image image, File imageFile, String name, ArrayList<Song> songs){
        this.image = image;
        this.imageFile = imageFile;
        this.name = name;
        this.songs = songs;
    }

    /**
     * Checks whether or not the object passed is equal to this playlist
     * @param o Object to check
     * @return True if object passed is equal to this playlist, otherwise returns false
     */
    public boolean equals(Object o){
        if(o == null) return false;
        if(!(o instanceof Playlist))
            return false;
        Playlist p = (Playlist)o;
        if(p.name.equals(this.name))
            return true;
        return false;
    }

    /**
     * Turns this object into its String object equivalent
     * @return String object equivalent to this object
     */
    public String toString(){
        return this.name;
    }

    /**
     * Reads this object in from user_session.dat file
     * @param s The stream to read from
     * @throws IOException Whenever imageFile cannot be created
     * @throws ClassNotFoundException Whenever reading this object fails due to serial UID mismatch
     */
    private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
        s.defaultReadObject();
        if(imageFile == null){
            image = null;
            return;
        }
        if(!imageFile.exists() || imageFile.length() == 0) {
            this.imageFile = null;
            this.image = null;
            return;
        }
        this.image = new Image(imageFile.toURI().toString());
    }

    /**
     * Gets this playlist's image
     * @return This playlist's image
     */
    public Image getImage(){
        return this.image;
    }

    /**
     * Gets this playlist's name
     * @return This playlist's name
     */
    public String getName(){
        return this.name;
    }

    /**
     * Gets this playlist's songs
     * @return This playlist's songs
     */
    public ArrayList<Song> getSongs(){
        return this.songs;
    }

    /**
     * Gets this playlist's image file
     * @return This playlist's image file
     */
    public File getImageFile(){
        return this.imageFile;
    }

    /**
     * Sets this playlist's image
     * @param image Image to set playlist's image to
     */
    public void setImage(Image image){
        this.image = image;
    }

    /**
     * Sets this playlist's name
     * @param name Name to set playlist's name to
     */
    public void setName(String name){
        this.name = name;
    }

    /**
     * Sets this playlist's list of songs
     * @param songs ArrayList of songs to set this playlist's list of songs to
     */
    public void setSongs(ArrayList<Song> songs){
        this.songs = songs;
    }

    /**
     * Creates a new local version of the image file provided (if it doesn't exist), and sets it as the image file of this playlist
     * @param externalImageFile Image file provided externally by user
     * @throws IOException Whenever local version of the image file cannot be created
     */
    public void setImageFile(File externalImageFile) throws IOException {
        if(externalImageFile == null){
            this.imageFile = null;
            this.image = null;
            return;
        }
        /* Get path of new local image localImageFile that will be created, then append the localImageFile name to the path and create it if it doesn't exist */
        File localImageFile = new File(User.imageDir + File.separator + externalImageFile.getName());
        if(!localImageFile.exists()) {
            localImageFile.createNewFile();
            InputStream externalImageStream = new FileInputStream(externalImageFile);
            int read = 0;
            byte[] externalImageData = new byte[(int) externalImageFile.length()];
            while (read < externalImageFile.length()) {
                read += externalImageStream.read(externalImageData);
            }
            externalImageStream.close();
            OutputStream localImageStream = new FileOutputStream(localImageFile);
            localImageStream.write(externalImageData);
            localImageStream.close();
        }
        this.imageFile = localImageFile;
        this.image = new Image(localImageFile.toURI().toString());
    }

    /**
     * Deletes the image file associated with this playlist if and only if there are no other playlists, and songs using it
     * @param playlistsSelectedForDeletion Playlists selected for deletion
     * @param file File to delete
     */
    public void deleteImageFile(ObservableList<Playlist> playlistsSelectedForDeletion, File file){
        if(imageFile == null)
            return;
        /* check for other playlists and songs in song library who use the image, if not delete it */
        boolean toDelete = true;
        /* check user song library to see if songs use the image */
        for(Song song : User.user.getSongLibrary()){
            if(song.getImageFile() != null) {
                if(file.equals(song.getImageFile()))
                    toDelete = false;
            }
        }
        /* check OTHER playlists to see who uses the image */
        for(Playlist userPlaylist : User.user.getPlaylists()){
            if(userPlaylist.getImageFile() == null)
                continue;
            if(!playlistsSelectedForDeletion.contains(userPlaylist) && userPlaylist.getImageFile().equals(file))
                toDelete = false;
        }
        if(toDelete) {
            file.delete();
            this.image = null;
            this.imageFile = null;
        }
    }
}
