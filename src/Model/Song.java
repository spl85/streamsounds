package Model;

import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import java.io.*;

/**
 * Class representation of a Song (has image, name, genre, audio, and composer)
 */
public class Song implements Serializable {

    /** Serial UID */
    private static final long serialVersionUID = 44L;

    /** Image of the song */
    private transient Image image;

    /** Name of the song */
    private String name;

    /** Genre of the song */
    private String genre;

    /** Composer of the song */
    private String composer;

    /** Audio file for the song */
    private File audioFile;

    /** Image file for the song */
    private File imageFile;

    /**
     * Constructs a song object with default null values
     */
    public Song(){
        this.image = null;
        this.name = "Song";
        this.genre = "N/A";
        this.composer = "N/A";
        this.audioFile = null;
        this.imageFile = null;
    }

    /**
     * Constructs a song object with predetermined values
     * @param i Image belonging to song
     * @param n Name of song
     * @param g Genre of song
     * @param c Composer of song
     * @param audioFile Local audio file belonging to the song
     * @param imageFile Local image file belonging to the song
     */
    Song(Image i, String n, String g, String c, File audioFile, File imageFile){
        this.image = i;
        this.name = n;
        this.genre = g;
        this.composer = c;
        this.audioFile = audioFile;
        this.imageFile = imageFile;
    }

    /**
     * Checks whether or not the object passed is equal to this song
     * @param o Object to check
     * @return True if object passed is equal to this song, otherwise returns false
     */
    public boolean equals(Object o){
        if(o == null) return false;
        if(!(o instanceof Song))
            return false;
        Song s = (Song)o;
        if(this.name.equals(s.name))
            return true;
         return false;
    }

    /**
     * Reads this object from user_session.dat
     * @param s Stream to read from
     * @throws IOException Whenever the image file cannot be created
     * @throws ClassNotFoundException Whenever reading this object fails due to serial UID mismatch
     */
    private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
        s.defaultReadObject();
        /* new object input stream to read data */
        if(imageFile == null){
            image = null;
            return;
        }
        if(!this.imageFile.exists()) {
            this.imageFile = null;
            this.image = null;
            return;
        }
        this.image = new Image(imageFile.toURI().toString());
    }

    /**
     * Gets this song's image
     * @return This song's image
     */
    public Image getImage(){
        return this.image;
    }

    /**
     * Gets this song's name
     * @return This song's name
     */
    public String getName(){
        return this.name;
    }

    /**
     * Gets this song's genre
     * @return This song's genre
     */
    public String getGenre(){
        return this.genre;
    }

    /**
     * Gets this song's composer
     * @return This song's composer
     */
    public String getComposer(){
        return this.composer;
    }

    /**
     * Gets this song's audio file
     * @return This song's audio file
     */
    public File getAudioFile(){ return this.audioFile; }

    /**
     * Gets this song's image file
     * @return This song's image file
     */
    public File getImageFile(){ return this.imageFile; }

    /**
     * Sets this song's name
     * @param name String object to set this song's name to
     */
    public void setName(String name){
        if(name.equals(""))
            this.name = "Song";
        else
            this.name = name;
    }

    /**
     * Sets this song's genre
     * @param genre String object to set this song's genre to
     */
    public void setGenre(String genre){
        if(genre.equals(""))
            this.genre = "N/A";
        else
            this.genre = genre;
    }

    /**
     * Sets this song's composer
     * @param composer String object to set this song's composer to
     */
    public void setComposer(String composer){
        if(composer.equals(""))
            this.composer = "N/A";
        else
            this.composer = composer;
    }

    /**
     * Sets the audio file field of this song; also reads bytes from the specified audio file into a new internal version of the file
     * @param externalAudioFile Audio file provided by user for this song
     * @throws IOException Whenever input stream cannot be opened
     */
    public void setAudioFile(File externalAudioFile) throws IOException {
        if(externalAudioFile == null) {
            this.audioFile = null;
            return;
        }
        /* get path of new internal version of audio file then create it if it doesn't exist */
        String localAudioURI = User.audioDir.concat(File.separator + externalAudioFile.getName());
        File localAudioFile = new File(localAudioURI);
        if(!localAudioFile.exists()) {
            localAudioFile.createNewFile();
            InputStream externalAudioStream = new FileInputStream(externalAudioFile);
            int read = 0;
            byte[] externalImageData = new byte[(int) externalAudioFile.length()];
            while (read < externalAudioFile.length()) {
                read += externalAudioStream.read(externalImageData);
            }
            externalAudioStream.close();
            OutputStream localAudioStream = new FileOutputStream(localAudioFile);
            localAudioStream.write(externalImageData);
            localAudioStream.close();
        }
        this.name = localAudioFile.getName().substring(0, localAudioFile.getName().lastIndexOf('.'));
        this.audioFile = localAudioFile;

    }

    /**
     * Creates a new local version of the image file provided (if it doesn't exist), and sets it as the image file of this song
     * @param externalImageFile Image file provided externally by user
     * @throws IOException Whenever local version of the image file cannot be created
     */
    public void setImageFile(File externalImageFile) throws IOException {
        /* get path of new local image file, then make a new file */
        if(externalImageFile == null) {
            this.imageFile = null;
            this.image = null;
            return;
        }
        String localImageURI = User.imageDir.concat(File.separator + externalImageFile.getName());
        File localImageFile = new File(localImageURI);
        if(!localImageFile.exists()) {
            localImageFile.createNewFile();
            InputStream externalImageStream = new FileInputStream(externalImageFile);
            int read = 0;
            byte[] externalImageData = new byte[(int) externalImageFile.length()];
            while (read < externalImageFile.length()) {
                read += externalImageStream.read(externalImageData);
            }
            externalImageStream.close();
            OutputStream localImageStream = new FileOutputStream(localImageFile);
            localImageStream.write(externalImageData);
            localImageStream.close();
        }
        this.imageFile = localImageFile;
        this.image = new Image(localImageFile.toURI().toString());
    }

    /**
     * Deletes the image file associated with this song if and only if there are no other playlists, and songs using it
     * @param songsSelectedForDeletion Songs selected for deletion
     * @param file File to delete
     */
    public void deleteImageFile(ObservableList<Song> songsSelectedForDeletion, File file){
        if(file == null)
            return;
        /* check for other playlists and songs in song library who use the image, if not delete it */
        boolean toDelete = true;
        /* check user song library to see if songs use the image */
        for(Song song : User.user.getSongLibrary()){
            if(!songsSelectedForDeletion.contains(song) && song.getImageFile() != null) {
                if(file.equals(song.getImageFile()))
                    toDelete = false;
            }
        }
        /* check OTHER playlists to see who uses the image */
        for(Playlist userPlaylist : User.user.getPlaylists()){
            if(userPlaylist.getImageFile() == null)
                continue;
            if(userPlaylist.getImageFile().equals(file))
                toDelete = false;
        }
        if(toDelete) {
            file.delete();
            this.imageFile = null;
            this.image = null;
        }
    }

    /**
     * Deletes the audio file associated with the songs selected for deletion
     * @param songsSelectedForDeletion Songs selected for deletion
     * @param file File to delete
     */
    public void deleteAudioFile(ObservableList<Song> songsSelectedForDeletion, File file){
        if(file == null)
            return;
        boolean toDelete = true;
        for(Song song : User.user.getSongLibrary()){
            if(song.getAudioFile() == null)
                continue;
            if(!song.equals(this) && song.getAudioFile().equals(file) && !songsSelectedForDeletion.contains(song))
                toDelete = false;
        }
        if(toDelete) {
            file.delete();
            this.audioFile = null;
        }
    }

    /**
     * Turns this object into its String object equivalent
     * @return String object equivalent to this object
     */
    public String toString() {
        return this.name;
    }

    /**
     * Turns this object into its long String object equivalent
     * @return Full String object equivalent to this object
     */
    public String toStringFull(){
        return this.name + " - " + this.composer + " - " + this.genre;
    }

}
