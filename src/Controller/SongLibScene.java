package Controller;

import Controller.Cells.ComposerTableCell;
import Controller.Cells.GenreTableCell;
import Controller.Cells.SongNameTableCell;
import Driver.StreamSounds;
import Model.Song;
import Model.User;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.input.*;
import javafx.stage.Modality;
import javafx.stage.Stage;
import java.io.IOException;
import java.util.stream.Collectors;

/**
 * Class that handles events on home.fxml -- includes an inner private class for ListCell design
 */
public class SongLibScene implements SongAddable {

    /** FXML injectables - protected */
    @FXML
    TextField searchTextField;
    @FXML
    TableView<Song> songTable;
    @FXML
    TableColumn<Song, String> songNameColumn;
    @FXML
    TableColumn<Song, String> composerColumn;
    @FXML
    TableColumn<Song, String> genreColumn;
    @FXML
    FontAwesomeIconView searchIcon;
    @FXML
    Label addSongLabel;

    /** Pointer to home controller which spawned this instanced */
    private Home home;

    /** Song clipboard that is populated during an on-drag-detected event from an instance of this controller
     * that is currently active */
    public static ObservableList<Song> songClipboard;

    /**
     * Initializes the controller with necessary fields, and initializes a pointer to the home controller
     * @param home Pointer to the home instance which spawned this scene
     */
    public void start(Home home){
        this.home = home;
        songClipboard = FXCollections.observableArrayList();
        addSongLabel.getStyleClass().add("add_playlist_label");
        songTable.setItems(home.getSongLibrary());
        songTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        searchTextField.textProperty().addListener((observable, oldValue, newValue) -> search());
        songNameColumn.setCellValueFactory(new PropertyValueFactory("name"));
        songNameColumn.setCellFactory(oldCell -> new SongNameTableCell(home));
        composerColumn.setCellValueFactory(new PropertyValueFactory("composer"));
        composerColumn.setCellFactory(oldCell -> new ComposerTableCell());
        genreColumn.setCellValueFactory(new PropertyValueFactory("genre"));
        genreColumn.setCellFactory(oldCell -> new GenreTableCell());
        songTable.setPlaceholder(new Label("No songs are currently present in your song library.\nTo add songs, click the + button above."));
        songTable.setOnMouseDragEntered(event -> handleDragEntered(event));
        songTable.setOnMouseReleased(event -> songTable.setCursor(Cursor.DEFAULT));
    }

    /**
     * Handles add song event
     * @throws IOException Whenever AddSongScene hierarchy cannot be loaded
     */
    public void addSong() throws IOException {
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/View/add_song.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        stage.setResizable(false);
        stage.setScene(scene);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(StreamSounds.primaryStage);
        stage.getIcons().add(new Image(getClass().getResource("/Icons/eighth-notes.png").toExternalForm()));
        stage.setTitle("Add Song(s) to Song Library");
        AddSongScene addSongScene = loader.getController();
        addSongScene.start(stage, this);
        stage.showAndWait();
    }

    /**
     * Adds specified song to the user's library by contacting the home controller which spawned this instance
     * @param song Song to add to user's song library
     */
    public void addSong(Song song){
        if(!home.getSongLibrary().contains(song)) {
            home.getSongLibrary().add(song);
            home.updateLists();
        }
        else
            return;
    }

    /**
     * On-drag-detected event that populates the static song clipboard field
     * @param event Mouse drag event that will initiate a copy of currently selected songs on the song table view
     */
    public void copySongs(MouseEvent event){
        songClipboard.setAll(songTable.getSelectionModel().getSelectedItems());
        songTable.startFullDrag();
        event.consume();
    }

    /**
     * Handles a drag event on this scene
     * @param event Mouse drag event enter on the songTable node
     */
    public void handleDragEntered(MouseDragEvent event){
        if(songClipboard.isEmpty())
            return;
        songTable.setCursor(Cursor.CLOSED_HAND);
        event.consume();
    }

    /**
     * Filters the current list of songs in the user's song library as the user types information into the searchTextField
     */
    public void search(){
        ObservableList<Song> songLibrary = FXCollections.observableArrayList(User.user.getSongLibrary());
        ObservableList<Song> list = songLibrary
                .stream()
                .filter(song -> song.toStringFull().toLowerCase().contains(searchTextField.getText().toLowerCase()))
                .collect(Collectors.toCollection(() -> FXCollections.observableArrayList()));
        songTable.setItems(list);
    }

}
