package Controller;

import Model.Playlist;
import Model.Song;
import Model.User;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import static javafx.scene.paint.Color.*;

/** Class that handles events on add_playlist.fxml */
public class AddPlaylistScene {

    /** FXML Injectables */
    @FXML
    TextField playlistNameTextField;
    @FXML
    Label playlistImagePathLabel;
    @FXML
    Button imageFinderButton;
    @FXML
    ImageView playlistImage;
    @FXML
    Button createPlaylistButton;
    @FXML
    FontAwesomeIconView defaultIcon;
    @FXML
    Label playlistNameLabel;

    /** Stage that this scene belongs to */
    Stage thisStage;

    /** Pointer to home controller for updates to lists and variable manipulation */
    Home home;

    /** Pointer to the latest image file that the user chose for their playlist */
    File latestImageFile;

    /**
     * Initializes controller
     * @param thisStage Stage that this scene belongs to
     * @param home Pointer to home controller for updates to lists and variable manipulations
     */
    public void start(Stage thisStage, Home home){
        this.home = home;
        this.thisStage = thisStage;
        setDefaultImage();
        FontAwesomeIconView folderIcon = new FontAwesomeIconView();
        folderIcon.setGlyphName("FOLDER_OPEN");
        folderIcon.setSize("14");
        folderIcon.setFill(GOLDENROD);
        imageFinderButton.setGraphic(folderIcon);
        latestImageFile = null;
    }

    /**
     * Sets a default image if the playlist's image is not chosen by the user
     */
    public void setDefaultImage(){
        playlistImage.setVisible(false);
        defaultIcon.setVisible(true);
        defaultIcon.setFill(BLACK);
    }

    /**
     * Handles event where the user wishes to add an image to a potential playlist
     */
    public void addImage() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Image File");
        fileChooser.getExtensionFilters().add(
                new FileChooser.ExtensionFilter("Image Files (*.jpg, *.png, *.bmp, *.gif",
                        "*.jpg", ".png", "*.bmp", "*.gif")
        );
        File imageFile = fileChooser.showOpenDialog(thisStage);
        if(imageFile == null){
            playlistImagePathLabel.setText("N/A");
            return;
        }
        latestImageFile = imageFile;
        playlistImage.setVisible(true);
        URI url = imageFile.toURI();
        Image image = new Image(url.toString());
        playlistImagePathLabel.setText(url.toString());
        playlistImage.setImage(image);
        defaultIcon.setVisible(false);
    }

    /**
     * Updates playlist name whenever user types into playlistNameTextField
     */
    public void updatePlaylistName(){
        if(playlistNameTextField.getText().equals(""))
            playlistNameLabel.setText("Playlist name");
        else
            playlistNameLabel.setText(playlistNameTextField.getText());
    }

    /**
     * Handles creation of a new playlist after user has inputted all their desired fields for this playlist
     * @throws IOException Whenever user's image file cannot be set
     */
    public void createPlaylist() throws IOException {
        Playlist playlist = new Playlist();
        playlist.setImageFile(latestImageFile);
        playlist.setName(playlistNameTextField.getText());
        if(playlist.getName().equals(""))
            playlist.setName("Playlist");
        if(!User.user.getPlaylists().contains(playlist)) {
            home.getPlaylists().add(playlist);
            thisStage.close();
        }
        else{
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Playlist Name Conflict");
            alert.setHeaderText("Playlist already exists");
            alert.setContentText("Rename playlist.");
            alert.showAndWait();
        }
    }
}
