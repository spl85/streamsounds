package Controller;


import animatefx.animation.FadeIn;
import animatefx.animation.FadeOut;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

/** Class that handles events on the MusicPlayerScene */
public class MusicPlayerScene {

    @FXML
    ImageView songImage;
    @FXML
    Label songNameLabel;
    @FXML
    Slider durationSlider;
    @FXML
    FontAwesomeIconView returnIcon;
    @FXML
    FontAwesomeIconView previousSongIcon;
    @FXML
    FontAwesomeIconView nextSongIcon;
    @FXML
    FontAwesomeIconView playIcon;
    @FXML
    BorderPane borderPane;

    /** Denotes whether or not a fade out animation can play */
    public boolean canFadeOut;

    /** Denotes whether or not a fade in animation is playing */
    public boolean fadeInPlaying;

    /** Pointer to home in order to access media player, current song playing, and play queue */
    Home home;

    /**
     * Initializes this scene with necessary fields
     * @param home Pointer to home in order to access media player, current song playing, and play queue
     */
    public void start(Home home){
        this.home = home;
        canFadeOut = true;
        fadeInPlaying = false;
        updateFXMLValues();
        home.thisRoot.setOnMouseMoved(event -> {
            if(!fadeInPlaying) {
                FadeIn fadeIn = new FadeIn(returnIcon);
                fadeIn.setSpeed(.3);
                FadeIn fadeIn2 = new FadeIn(nextSongIcon);
                fadeIn2.setSpeed(.3);
                FadeIn fadeIn3 = new FadeIn(previousSongIcon);
                fadeIn3.setSpeed(.3);
                FadeIn fadeIn4 = new FadeIn(playIcon);
                fadeIn4.setSpeed(.3);
                fadeIn.play();
                fadeIn2.play();
                fadeIn3.play();
                fadeIn4.play();
                fadeInPlaying = true;
                fadeIn.setOnFinished(event1 -> {
                    if (canFadeOut) {
                        FadeOut fadeOut = new FadeOut();
                        fadeOut.setNode(returnIcon);
                        fadeOut.play();
                        fadeInPlaying = false;
                    }
                });
                fadeIn2.setOnFinished(event1 -> {
                    if (canFadeOut) {
                        FadeOut fadeOut = new FadeOut();
                        fadeOut.setNode(fadeIn2.getNode());
                        fadeOut.play();
                        fadeInPlaying = false;
                    }
                });
                fadeIn3.setOnFinished(event1 -> {
                    if (canFadeOut) {
                        FadeOut fadeOut = new FadeOut();
                        fadeOut.setNode(fadeIn3.getNode());
                        fadeOut.play();
                        fadeInPlaying = false;
                    }
                });
                fadeIn4.setOnFinished(event1 -> {
                    if (canFadeOut) {
                        FadeOut fadeOut = new FadeOut();
                        fadeOut.setNode(fadeIn4.getNode());
                        fadeOut.play();
                        fadeInPlaying = false;
                    }
                });
            }
        });
        AnchorPane pane = (AnchorPane)songImage.getParent();
        songImage.fitHeightProperty().bind(pane.heightProperty());
        songImage.fitWidthProperty().bind(pane.widthProperty());
    }

    /**
     * Handles a mouse over event on a control node
     */
    public void fadeOutFalse(){
        canFadeOut = false;
        fadeInPlaying = true;
    }

    /**
     * Handles mouse exit event on a control node
     */
    public void fadeOutTrue(){
        canFadeOut = true;
        fadeInPlaying = false;
    }

    /**
     * Updates FXML values pertaining to this particular scened
     */
    public void updateFXMLValues(){
        songImage.setImage(home.getSongPlaying().getImage());
        durationSlider.setValue(home.durationSlider.getValue());
        durationSlider.valueProperty().bindBidirectional(home.durationSlider.valueProperty());
        durationSlider.maxProperty().bindBidirectional(home.durationSlider.maxProperty());
        durationSlider.setMax(home.durationSlider.getMax());
        durationSlider.setOnMouseClicked(home.durationSlider.getOnMouseClicked());
        durationSlider.setOnMousePressed(home.durationSlider.getOnMousePressed());
        durationSlider.setOnMouseReleased(home.durationSlider.getOnMouseReleased());
        songImage.imageProperty().bind(home.songImage.imageProperty());
        songNameLabel.textProperty().bind(home.songNameLabel.textProperty());
        playIcon.glyphNameProperty().bind(home.playIcon.glyphNameProperty());
        playIcon.setOnMouseClicked(home.playIcon.getOnMouseClicked());
        songNameLabel.getStyleClass().add("music_player_text");
    }

    /**
     * Handles a user's request to return to the home screen
     */
    public void closeMusicPlayer(){
        home.closeMusicPlayer();
    }

    /**
     * Handles next next song button event
     */
    public void nextSong(){
        home.nextSong();
    }

    /**
     * Handles previous song button event
     */
    public void previousSong(){
        home.previousSong();
    }

}
