package Controller;

import Controller.Cells.PlaylistSongCell;
import Driver.StreamSounds;
import Model.Playlist;
import Model.Song;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import java.io.IOException;
import java.util.stream.Collectors;

/** Class that handles events on PlaylistScene */
public class PlaylistScene implements Controller.SongAddable {

    /** FXML injectables - protected */
    @FXML
    ImageView playlistImage;
    @FXML
    Label playlistNameLabel;
    @FXML
    ListView<Song> songsListView;
    @FXML
    TextField searchTextField;
    @FXML
    Label addSongLabel;
    @FXML
    FontAwesomeIconView defaultIcon;

    /** List of playlist's songs */
    ObservableList<Song> songsList;

    /** Playlist that is currently selected on Home screen */
    Playlist playlist;

    /** Pointer to the home controller instance which spawned this instance of PlaylistScene */
    private Controller.Home home;

    /**
     * Initializes this scene with the necessary fields
     * @param home Pointer to home instance which spawned this scene
     * @param p Playlist that the user has selected on the home screen
     */
    public void start(Home home, Playlist p){
        /* if p is null there is no playlist selected on home screen, and the user clicked to go to the song library */
        if(p == null) {
            addSongLabel.setDisable(true);
            return;
        }
        this.playlist = p;
        this.home = home;
        songsList = FXCollections.observableArrayList(playlist.getSongs());
        songsListView.setCellFactory(replaceDefaultListCellWith -> new PlaylistSongCell(home));
        songsListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        songsListView.setItems(songsList);
        addSongLabel.getStyleClass().add("add_playlist_label");
        searchTextField.textProperty().addListener((observable, oldValue, newValue) -> search());
        playlistNameLabel.setText(p.getName());
        if(playlist.getImage() != null) {
            playlistImage.setSmooth(true);
            playlistImage.setImage(p.getImage());
        }
        else{
            defaultIcon.setVisible(true);
            playlistImage.setVisible(false);
        }
    }

    /**
     * Handles user's request to add a song to this playlist
     * @throws IOException Whenever loader cannot load AddSongScene hierarchy
     */
    public void addSong() throws IOException {
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/View/add_song.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        stage.setResizable(false);
        stage.setScene(scene);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(StreamSounds.primaryStage);
        stage.getIcons().add(new Image(getClass().getResource("/Icons/eighth-notes.png").toExternalForm()));
        stage.setTitle("Add Song/Songs to " + playlist.getName());
        AddSongScene addSongScene = loader.getController();
        addSongScene.start(stage, this);
        stage.showAndWait();
    }

    /**
     * Filters the playlist's list of songs as the user types information into the searchTextField
     */
    public void search(){
        ObservableList<Song> playlistSongs = FXCollections.observableArrayList(playlist.getSongs());
        ObservableList<Song> list = playlistSongs
                .stream()
                .filter(song -> song.toStringFull().toLowerCase().contains(searchTextField.getText().toLowerCase()))
                .collect(Collectors.toCollection(() -> FXCollections.observableArrayList()));
        songsListView.setItems(list);
    }

    /**
     * Adds song to user's currently selected playlist by adding it to the current list of songs then updating the playlist's songs
     * @param song Song to add to currently selected playlist
     */
    public void addSong(Song song) {
        if(home.getSongLibrary().contains(song))
            home.getSongLibrary().remove(home.getSongLibrary().indexOf(song));
        home.getSongLibrary().add(song);
        if(playlist.getSongs().contains(song)) {
            playlist.getSongs().remove(song);
            playlist.getSongs().add(song);
        } else{
            playlist.getSongs().add(song);
        }
        if(songsList.contains(song))
            songsList.remove(song);
        songsList.add(song);
        home.updateLists();
    }
}
