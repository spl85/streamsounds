package Controller;

import Controller.Cells.PlaylistCell;
import Driver.StreamSounds;
import Model.Playlist;
import Model.Song;
import Model.User;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import java.io.IOException;
import java.util.List;

/**
 * Class that handles events on home.fxml
 */
public class Home {

    /** FXML injectables */
    @FXML
    FontAwesomeIconView playIcon;
    @FXML
    FontAwesomeIconView backIcon;
    @FXML
    FontAwesomeIconView forwardIcon;
    @FXML
    FontAwesomeIconView musicPlayerIcon;
    @FXML
    FontAwesomeIconView volumeIcon;
    @FXML
    FontAwesomeIconView stopIcon;
    @FXML
    ImageView songImage;
    @FXML
    Label songLibraryLabel;
    @FXML
    Label addPlaylistLabel;
    @FXML
    Label songNameLabel;
    @FXML
    Label currentDurationLabel;
    @FXML
    Label totalDurationLabel;
    @FXML
    Slider durationSlider;
    @FXML
    Slider volumeSlider;
    @FXML
    public ListView<Playlist> playlistListView;
    @FXML
    SplitPane splitPane;
    @FXML
    AnchorPane nestedUI;

    /** The root of this loaded FXML hierarchy */
    AnchorPane thisRoot;

    /** Saved home state before entering music player scene */
    private ObservableList<Node> savedHomeState;

    /** User's list of songs in their song library */
    private ObservableList<Song> songLibrary;

    /** User's list of playlists */
    private ObservableList<Playlist> playlists;

    /** Music player belonging to this scene */
    protected MediaPlayer musicPlayer;

    /** Songs currently in queue to play */
    protected List<Song> playQueue;

    /** Current song playing on home screen */
    protected Song songPlaying;

    /** Pointer to self so fields of currently active controller can be modified from static context */
    private static Home home;

    /**
     * Initializes controller by adding listeners, and initializing fields -- populates current song from mps if not null
     * otherwise sets currentSong to null
     * @throws IOException Whenever SongLibScene hierarchy cannot be loaded
     */
    public void start() throws IOException {
        home = this;
        StreamSounds.primaryStage.getScene().getStylesheets().add("Styles/BaseStyle.css");
        songNameLabel.setStyle("-fx-font-size: 14");
        songLibraryLabel.getStyleClass().add("song_library_label");
        addPlaylistLabel.getStyleClass().add("add_playlist_label");
        Tooltip tt = new Tooltip();
        tt.setText("Music player");
        tt.setStyle("-fx-font-size: 10");
        Tooltip.install(musicPlayerIcon, tt);
        FXMLLoader songLibRootLoader = new FXMLLoader();
        songLibRootLoader.setLocation(getClass().getResource("/View/songlib_scene.fxml"));
        Parent songLibRoot = songLibRootLoader.load();
        ObservableList<Node> songLibraryChildren = songLibRoot.getChildrenUnmodifiable();
        nestedUI.getChildren().addAll(songLibraryChildren);
        SongLibScene sls = songLibRootLoader.getController();
        playlists = FXCollections.observableArrayList();
        playlists.addAll(User.user.getPlaylists());
        songLibrary = FXCollections.observableArrayList();
        songLibrary.addAll(User.user.getSongLibrary());
        playQueue = songLibrary;
        songPlaying = null;
        playlistListView.setItems(playlists);
        sls.start(this);
        home.volumeSlider.setMax(1);
        home.volumeSlider.setValue(0.5);
        volumeSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue.doubleValue() == 0)
                volumeIcon.setGlyphName("VOLUME_OFF");
            else
                volumeIcon.setGlyphName("VOLUME_UP");
        });
        playlistListView.setCellFactory(oldCell -> new PlaylistCell());
        playlistListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        playlistListView.getSelectionModel().selectedIndexProperty().addListener(((observable, oldValue, newValue) -> {
            try {
                viewPlaylist();
            } catch (IOException e) {
            }
        }));
    }

    /**
     * Handles play requests sent by anyone by processing them, and creating a new media player whose media value
     * is attained from the requested song's audio file, also updates the current play queue depending where the request
     * came from
     * @param song Song to play
     * @param playQueue Queue initialized by sender of request
     */
    public static void handlePlayRequest(Song song, List<Song> playQueue){
        if(home.songPlaying == null) {
            home.setupNewPlayer(song);
        }
        else if(!home.songPlaying.equals(song) && home.musicPlayer != null) {
            home.songPlaying = song;
            home.musicPlayer.stop();
        }
        /* update play queue */
        home.playQueue = playQueue;
    }

    /**
     * Initializes a new media player
     * @param song Song to initialize media, and MediaPlayer object to
     * @return New MediaPlayer object that is initialized with Media object initialized from song's audio file
     */
    public MediaPlayer setupNewPlayer(Song song){
        Media media = new Media(song.getAudioFile().toURI().toString());
        MediaPlayer requestedSongPlayer = new MediaPlayer(media);
        requestedSongPlayer.setOnReady(()->{
            home.musicPlayer = requestedSongPlayer;
            songImage.setImage(song.getImage());
            home.songNameLabel.setText(song.getName());
            String totalDuration = getDisplayableDuration(requestedSongPlayer.getTotalDuration());
            home.totalDurationLabel.setText(totalDuration);
            home.volumeSlider.setValue(home.volumeSlider.getValue());
            requestedSongPlayer.volumeProperty().bind(home.volumeSlider.valueProperty());
            home.durationSlider.setMax(requestedSongPlayer.getTotalDuration().toMillis());
            home.durationSlider.setOnMouseClicked(event -> home.musicPlayer.seek(new Duration(home.durationSlider.getValue())));
            home.durationSlider.setOnMousePressed(event -> home.durationSlider.setValueChanging(true));
            home.durationSlider.setOnMouseReleased(event -> {
                home.musicPlayer.seek(new Duration(home.durationSlider.getValue()));
                home.durationSlider.setValueChanging(false);
            });
            home.durationSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
                long newValueLong = newValue.longValue();
                home.currentDurationLabel.setText(getDisplayableDuration(new Duration(newValueLong)));
            });
            requestedSongPlayer.currentTimeProperty().addListener(((observable, oldValue, newValue) -> {
                if(!home.durationSlider.isValueChanging()){
                    home.durationSlider.setValue(newValue.toMillis());
                }
            }));
            home.musicPlayer.play();
            home.playIcon.setGlyphName("PAUSE");
            home.songPlaying = song;
        });

        requestedSongPlayer.setOnPlaying(()->{
            home.forwardIcon.setDisable(false);
            home.backIcon.setDisable(false);
            playIcon.setGlyphName("PAUSE");
        });

        requestedSongPlayer.setOnPaused(()->playIcon.setGlyphName("PLAY"));

        requestedSongPlayer.setOnStopped(()->{
            songImage.setImage(null);
            home.songNameLabel.setText("--");
            home.totalDurationLabel.setText("--");
            home.durationSlider.setValue(0);
            requestedSongPlayer.volumeProperty().unbind();
            home.musicPlayer.dispose();
            if(home.songPlaying != null)
            home.musicPlayer = home.setupNewPlayer(home.songPlaying);
            if(!playIcon.getGlyphName().equals("PLAY"))
                playIcon.setGlyphName("PLAY");
        });

        requestedSongPlayer.setOnEndOfMedia(this::nextSong);
        return requestedSongPlayer;
    }

    /**
     * Gets a displayable duration
     * @param duration Duration to convert into a displayable time
     * @return String object in the form hh:mm:ss (form varies on situation)
     */
    private String getDisplayableDuration(Duration duration){
        String hours, minutes, seconds, totalDuration;
        if(duration.toHours() < 10 && duration.toHours() >= 1)
            hours = "0" + (int)duration.toHours() + " : ";
        else if(duration.toHours() >= 10)
            hours = "" + (int) duration.toHours() + " : ";
        else
            hours = "";
        if(duration.toMinutes() % 60 < 10 && !hours.equals(""))
            minutes = "0" + (int)duration.toMinutes() % 60 + " : ";
        else
            minutes = "" + (int)duration.toMinutes() % 60 + " : ";
        if(duration.toSeconds() % 60 < 10)
            seconds = "0" + (int)duration.toSeconds() % 60;
        else
            seconds = "" + (int)duration.toSeconds() % 60;
        totalDuration = hours + minutes + seconds;
        return totalDuration;
    }

    /**
     * Handles play button presses
     */
    public void handlePlayButton(){
        if(home.songPlaying == null) {
            if(!home.songLibrary.isEmpty()) {
                home.backIcon.setDisable(true);
                home.forwardIcon.setDisable(true);
                handlePlayRequest(home.songLibrary.get(0), home.songLibrary);
            }
        } else{
            if(home.playIcon.getGlyphName().equals("PAUSE"))
                home.musicPlayer.pause();
            else
                home.musicPlayer.play();
        }
    }

    /**
     * Handles stop button presses
     */
    public void stopSong(){
        if(home.musicPlayer != null) {
            home.songPlaying = null;
            home.musicPlayer.stop();
        }
    }

    /**
     * Handles previous button presses
     */
    public void previousSong(){
        if(home.musicPlayer != null){
            try{
                Song song = home.playQueue.get(home.playQueue.indexOf(songPlaying) - 1);
                home.backIcon.setDisable(true);
                home.forwardIcon.setDisable(true);
                handlePlayRequest(song, home.playQueue);
            } catch(IndexOutOfBoundsException e){
                home.songPlaying = null;
                home.musicPlayer.stop();
            }
        }
    }

    /**
     * Handles next song button presses
     */
    public void nextSong(){
        if(home.musicPlayer != null){
            try{
                Song song = home.playQueue.get(home.playQueue.indexOf(songPlaying) + 1);
                home.backIcon.setDisable(true);
                home.forwardIcon.setDisable(true);
                handlePlayRequest(song, home.playQueue);
            } catch(IndexOutOfBoundsException e){
                home.songPlaying = null;
                home.musicPlayer.stop();
                /* end of queue */
            }
        }
    }

    /**
     * Handles mute song button presses
     */
    public void muteSong(){
        if(home.volumeIcon.getGlyphName().equals("VOLUME_UP"))
            home.volumeSlider.setValue(0);
        else
            home.volumeSlider.setValue(0.4);
    }

    /**
     * Opens music player, and sets this root's children with a newly loaded music player scene hierarchy
     * @throws IOException Whenever scene cannot be loaded
     */
    public void openMusicPlayer() throws IOException {
        /* load music player with current song */
        if(songPlaying == null)
            return;
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/View/music_player.fxml"));
        Parent root = loader.load();
        thisRoot = (AnchorPane)musicPlayerIcon.getParent();
        savedHomeState = FXCollections.observableArrayList(thisRoot.getChildren());
        thisRoot.getChildren().setAll(root.getChildrenUnmodifiable());
        thisRoot.getStyleClass().add("anchor_pane");
        MusicPlayerScene mps = loader.getController();
        mps.start(home);
    }

    /**
     * Closes the music player scene by resetting the root's children with a saved node list
     */
    public void closeMusicPlayer(){
        thisRoot.getStyleClass().setAll("root");
        thisRoot.getChildren().setAll(savedHomeState);
    }

    /**
     * Gets current song that is playing on this scene
     * @return Song playing on this scene
     */
    public Song getSongPlaying() { return songPlaying; }

    /**
     * Handles add playlist event
     * @throws IOException Whenever AddPlaylistScene's hierarchy cannot be loaded
     */
    public void addPlaylist() throws IOException {
        FXMLLoader addPlaylistLoader = new FXMLLoader();
        addPlaylistLoader.setLocation(getClass().getResource("/View/add_playlist.fxml"));
        Parent root = addPlaylistLoader.load();
        AddPlaylistScene addPlaylistScene = addPlaylistLoader.getController();
        Stage stage = new Stage();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.initOwner(StreamSounds.primaryStage);
        stage.getIcons().add(new Image(getClass().getResource("/Icons/eighth-notes.png").toExternalForm()));
        stage.initModality(Modality.WINDOW_MODAL);
        stage.setResizable(false);
        addPlaylistScene.start(stage, this);
        stage.showAndWait();
        updateLists();
    }

    /**
     * Updates user's fields every time a change occurs to either a song library list or playlist list
     */
    public void updateLists(){
        if(User.user == null)
            return;
        User.user.getPlaylists().clear();
        User.user.getPlaylists().addAll(playlists);
        User.user.getSongLibrary().clear();
        User.user.getSongLibrary().addAll(songLibrary);
    }

    /**
     * Loads the song library sub-UI
     * @throws IOException Whenever loader cannot load controller
     */
    public void goToSongLibrary() throws IOException {
        playlistListView.getSelectionModel().clearSelection();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/View/songlib_scene.fxml"));
        Parent songlibRoot = loader.load();
        ObservableList<Node> songlibChildren = songlibRoot.getChildrenUnmodifiable();
        nestedUI.getChildren().setAll(songlibChildren);
        SongLibScene sls = loader.getController();
        sls.start(this);
        sls.searchTextField.clear();
        updateLists();
    }

    /**
     * Loads the playlist sub-UI
     * @throws IOException Whenever loader cannot load controller
     */
    private void viewPlaylist() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/View/playlist_scene.fxml"));
        Parent playlistRoot = loader.load();
        ObservableList<Node> playlistChildren = playlistRoot.getChildrenUnmodifiable();
        nestedUI.getChildren().setAll(playlistChildren);
        PlaylistScene pls = loader.getController();
        pls.start(this, playlistListView.getSelectionModel().getSelectedItem());
        updateLists();
    }

    /**
     * Gets the user's current list of songs in their song library
     * @return User's current list of songs in their song library
     */
    public ObservableList<Song> getSongLibrary(){
        return this.songLibrary;
    }

    /**
     * Sets user's current list of songs in their song library to the one provided
     * @param songLibrary List of songs to set the user's song library to
     */
    public void setSongLibrary(ObservableList<Song> songLibrary){
        this.songLibrary = songLibrary;
    }

    /**
     * Gets the user's current list of playlists
     * @return User's current list of playlists
     */
    public ObservableList<Playlist> getPlaylists(){
        return this.playlists;
    }

    /**
     * Sets user's current list of playlists to the one provided
     * @param playlists List of playlists to set the user's to
     */
    public void setPlaylists(ObservableList<Playlist> playlists){
        this.playlists = playlists;
    }
}
