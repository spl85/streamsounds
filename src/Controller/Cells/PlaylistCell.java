package Controller.Cells;

import Controller.SongLibScene;
import Model.Playlist;
import Model.Song;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.stage.FileChooser;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/** Class that defines a playlist cell */
public class PlaylistCell extends ListCell<Playlist> {

    /** Text field to input the playlist's new name */
    TextField editNameField;

    /**
     * Constructs a new PlaylistCell object
     */
    public PlaylistCell(){
        getStyleClass().add("playlist_cell");
        setEditable(false);
        editNameField = new TextField();
        editNameField.getStyleClass().add("standard-text-field");
    }

    /**
     * Updates the context menu when the user clicks on the cell, and sets up a potential edit
     */
    public void updateContextMenu(){
        boolean multipleSelected;
        if(getListView().getSelectionModel().getSelectedIndices().size() > 1)
            multipleSelected = true;
        else
            multipleSelected = false;
        MenuItem rename;
        rename = new MenuItem("Rename");
        FontAwesomeIconView renameIcon = new FontAwesomeIconView();
        renameIcon.setGlyphName("STRIKETHROUGH");
        rename.setGraphic(renameIcon);
        rename.setOnAction(event -> {
            setEditable(true);
            startEdit();
        });
        String changeImageOption = "Change Image";
        if(multipleSelected)
            changeImageOption += "s";
        MenuItem changeImage = new MenuItem(changeImageOption);
        FontAwesomeIconView changeImageIcon = new FontAwesomeIconView();
        changeImageIcon.setGlyphName("EXCHANGE");
        changeImage.setGraphic(changeImageIcon);
        changeImage.setOnAction(event -> {
            try {
                changeImage();
            } catch (IOException e) {
            }
        });
        String deleteOption = "Delete Playlist";
        if(multipleSelected)
            deleteOption += "s";
        MenuItem delete = new MenuItem(deleteOption);
        FontAwesomeIconView deleteIcon = new FontAwesomeIconView();
        deleteIcon.setGlyphName("TRASH");
        delete.setGraphic(deleteIcon);
        delete.setOnAction(event -> deletePlaylists());
        ContextMenu menu;
        if(!multipleSelected)
            menu = new ContextMenu(rename, changeImage, delete);
        else
            menu = new ContextMenu(changeImage, delete);
        setContextMenu(menu);
    }

    /**
     * Changes image of a playlist
     * @throws IOException Whenever the new image file cannot be set
     */
    public void changeImage() throws IOException {
        FileChooser imageChooser = new FileChooser();
        imageChooser.setTitle("Set New Image File");
        imageChooser.getExtensionFilters().add(
                new FileChooser.ExtensionFilter("Image Files (*.jpg, *.png, *.bmp, *.gif",
                        "*.jpg", "*.png", "*.bmp", "*.gif")
        );
        File file = imageChooser.showOpenDialog(getScene().getWindow());
        if(file == null)
            return;
        ObservableList<Playlist> playlistsSelected = getListView().getSelectionModel().getSelectedItems();
        for(Playlist p : playlistsSelected){
            ArrayList<Song> songsInPlaylist = p.getSongs();
            File oldImageFile = p.getImageFile();
            File newImageFile = file;
            for(Song song : songsInPlaylist){
                if(song.getImageFile() == null){
                    song.setImageFile(newImageFile);
                    continue;
                }
                if(song.getImageFile().equals(oldImageFile)){
                    song.deleteImageFile(FXCollections.observableArrayList(song), oldImageFile);
                    song.setImageFile(newImageFile);
                }
            }
            p.deleteImageFile(playlistsSelected, p.getImageFile());
            p.setImageFile(newImageFile);
        }
        getListView().refresh();
        getListView().getSelectionModel().clearSelection();
        getListView().getSelectionModel().select(getItem());
    }

    /**
     * Deletes selected playlists
     */
    public void deletePlaylists(){
        ObservableList<Playlist> playlistsSelected = getListView().getSelectionModel().getSelectedItems();
        for(Playlist p : playlistsSelected){
            p.deleteImageFile(playlistsSelected, p.getImageFile());
        }
        getListView().getItems().removeAll(playlistsSelected);
    }

    /**
     * Wraps up, or cancels an edit
     */
    @Override
    public void cancelEdit(){
        super.cancelEdit();
        updateItem(getItem(), isEmpty());
        setStyle(null);
        setEditable(false);
    }

    /**
     * Commits the edit by accessing the playlist text field's text and setting the new playlist's name with its value
     * @param newValue New value to commit
     */
    @Override
    public void commitEdit(Playlist newValue) {
        newValue.setName(editNameField.getText());
        super.commitEdit(newValue);
        setEditable(false);
        ListView<Playlist> listView = getListView();
        listView.getSelectionModel().clearSelection();
        listView.getSelectionModel().select(getItem());
        setStyle(null);
    }

    /**
     * Starts editing of a genre table cell
     */
    @Override
    public void startEdit() {
        if(!isEditable())
            return;
        setStyle("-fx-cell-size: 88");
        super.startEdit();
        setText(null);
        setGraphic(editNameField);
        editNameField.setText(getItem().getName());
        editNameField.requestFocus();
        editNameField.focusedProperty().addListener((observable, oldVal, newVal) -> {
            if(!newVal)
                cancelEdit();
        });
        editNameField.selectAll();
    }

    /**
     * Updates the item that belongs to this table cell
     * @param item Item to update
     * @param empty True if the cell is empty, false otherwise
     */
    @Override
    protected void updateItem(Playlist item, boolean empty){
        super.updateItem(item, empty);
        if(empty || item == null){
            setText(null);
            setGraphic(null);
        } else {
            getStyleClass().add("standard_label");
            setOnMouseClicked(event -> updateContextMenu());
            setOnKeyPressed(event -> {
                KeyEvent keyEvent = event;
                if(keyEvent.getCode() == KeyCode.ENTER)
                    commitEdit(getItem());
            });
            setOnMouseDragReleased(event -> {
                try {
                    handleDragDrop();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            setOnMouseDragEntered(event -> setStyle("-fx-border-color: BLUE;\n-fx-background-color: SILVER;"));
            setOnMouseDragExited(event -> setStyle(null));
            setText(item.toString());
            if(item.getImage() != null) {
                setGraphicTextGap(15);
                ImageView image = new ImageView(item.getImage());
                image.setFitWidth(100);
                image.setFitHeight(80);
                image.setPreserveRatio(false);
                image.setSmooth(true);
                setGraphic(image);
            } else{
                setGraphicTextGap(30);
                FontAwesomeIconView icon = new FontAwesomeIconView();
                icon.setGlyphName("MUSIC");
                icon.setSize("80");
                setGraphic(icon);
            }
        }
    }

    /**
     * Handles a drag drop event when user wishes to drop songs from the song library into this playlist
     * @throws IOException Whenever image file of either song or playlist cannot be set
     */
    public void handleDragDrop() throws IOException {
        if(SongLibScene.songClipboard.isEmpty())
            return;
        Playlist thisPlaylist = getItem();
        for(Song songDropped : SongLibScene.songClipboard){
            if(!thisPlaylist.getSongs().contains(songDropped)) {
                if(songDropped.getImageFile() == null)
                    songDropped.setImageFile(thisPlaylist.getImageFile());
                thisPlaylist.getSongs().add(songDropped);
            }
        }
        getListView().getSelectionModel().select(thisPlaylist);
    }
}
