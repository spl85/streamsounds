package Controller.Cells;

import Model.Song;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.geometry.Pos;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableCell;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

/** Class that defines a genre cell */
public class GenreTableCell extends TableCell<Song, String> {

    /** Text field to input the song's new genre */
    TextField editGenreTextField;

    /**
     * Constructs a new GenreTableCell object
     */
    public GenreTableCell(){
        setAlignment(Pos.CENTER_LEFT);
        editGenreTextField = new TextField();
        editGenreTextField.getStyleClass().add("standard-text-field");
        setEditable(false);
    }

    /**
     * Updates the context menu when the user clicks on the cell, and sets up a potential edit
     */
    public void updateContextMenu(){
        boolean multipleSelected = false;
        if(getTableView().getSelectionModel().getSelectedIndices().size() > 1)
            multipleSelected = true;
        ContextMenu menu = new ContextMenu();
        String genreOption = "Change Genre";
        if(multipleSelected)
            genreOption += "s";
        MenuItem changeGenre = new MenuItem(genreOption);
        changeGenre.setOnAction(event -> {
            setEditable(true);
            startEdit();
        });
        FontAwesomeIconView changeGenreIcon = new FontAwesomeIconView();
        changeGenreIcon.setGlyphName("STRIKETHROUGH");
        changeGenre.setGraphic(changeGenreIcon);
        menu.getItems().add(changeGenre);
        setContextMenu(menu);
    }

    /**
     * Starts editing of a genre table cell
     */
    @Override
    public void startEdit() {
        if(!isEditable())
            return;
        getTableRow().setStyle("-fx-cell-size: 67");
        getTableRow().startEdit();
        setText(null);
        setGraphic(editGenreTextField);
        setAlignment(Pos.CENTER_LEFT);
        editGenreTextField.setText(getItem());
        editGenreTextField.requestFocus();
        editGenreTextField.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!newValue)
                cancelEdit();
        });
        editGenreTextField.selectAll();
    }

    /**
     * Commits the edit by accessing the genre text field's text and setting the new song genre with its value
     * @param newValue New value to commit
     */
    @Override
    public void commitEdit(String newValue) {
        newValue = editGenreTextField.getText();
        for(Song song : getTableView().getSelectionModel().getSelectedItems())
            song.setGenre(newValue);
        setEditable(false);
        getTableRow().setStyle(null);
        getTableView().refresh();
        cancelEdit();
    }

    /**
     * Wraps up, or cancels an edit
     */
    @Override
    public void cancelEdit() {
        getTableRow().cancelEdit();
        updateItem(getItem(), isEmpty());
        setStyle(null);
        getTableRow().setStyle(null);
        setEditable(false);
    }

    /**
     * Updates the item that belongs to this table cell
     * @param item Item to update
     * @param empty True if the cell is empty, false otherwise
     */
    @Override
    protected void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);
        if(empty || item == null){
            setOnMouseClicked(null);
            setOnKeyPressed(null);
            setGraphic(null);
            setText(null);
        } else{
            setOnMouseClicked(event -> updateContextMenu());
            setOnKeyPressed(event -> {
                KeyEvent keyEvent = event;
                if(keyEvent.getCode() == KeyCode.ENTER)
                    commitEdit(editGenreTextField.getText());
            });
            Song song = (Song)getTableRow().getItem();
            if(song == null)
                return;
            setGraphic(null);
            setText(song.getGenre());
        }
    }
}
