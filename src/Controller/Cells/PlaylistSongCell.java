package Controller.Cells;

import Controller.Home;
import Model.Song;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.stage.FileChooser;
import java.io.File;
import java.io.IOException;

/** Class that further defines the layout of a list of songs belonging to a playlist */
public class PlaylistSongCell extends ListCell<Song> {

    /** Text field to input the playlist's song's new name */
    TextField editNameTextField;

    /** Pointer to the home instance which owns this song cell */
    Home home;

    /**
     * Returns instances of this cell
     * @param home Pointer to the home instance which owns this song cell
     */
    public PlaylistSongCell(Home home){
        this.home = home;
        setEditable(false);
        editNameTextField = new TextField();
        editNameTextField.getStyleClass().add("standard-text-field");
    }

    /**
     * Updates the context menu when the user clicks on the cell, and sets up a potential edit
     */
    public void updateContextMenu(){
        boolean multipleSelected = false;
        if(getListView().getSelectionModel().getSelectedIndices().size() > 1)
            multipleSelected = true;
        ContextMenu menu = new ContextMenu();
        MenuItem play = new MenuItem("Play");
        play.setOnAction(event -> Home.handlePlayRequest(getItem(), home.playlistListView.getSelectionModel().getSelectedItem().getSongs()));
        FontAwesomeIconView playIcon = new FontAwesomeIconView();
        playIcon.setGlyphName("PLAY");
        play.setGraphic(playIcon);
        MenuItem rename = new MenuItem("Rename Song");
        FontAwesomeIconView renameIcon = new FontAwesomeIconView();
        renameIcon.setGlyphName("STRIKETHROUGH");
        rename.setGraphic(renameIcon);
        rename.setOnAction(event -> {
            setEditable(true);
            startEdit();
        });
        String changeImageOption = "Change Image";
        if(multipleSelected)
            changeImageOption += "s";
        MenuItem changeImage = new MenuItem(changeImageOption);
        FontAwesomeIconView changeImageIcon = new FontAwesomeIconView();
        changeImageIcon.setGlyphName("EXCHANGE");
        changeImage.setGraphic(changeImageIcon);
        changeImage.setOnAction(event -> {
            try {
                changeImage();
            } catch (IOException e) {
            }
        });
        MenuItem remove = new MenuItem("Remove From Playlist");
        FontAwesomeIconView removeIcon = new FontAwesomeIconView();
        removeIcon.setGlyphName("TRASH");
        remove.setGraphic(removeIcon);
        remove.setOnAction(event -> removeSong());
        if(!multipleSelected)
            menu.getItems().addAll(play, rename, changeImage, remove);
        else
            menu.getItems().addAll(changeImage, remove);
        setContextMenu(menu);
    }

    /**
     * Changes image of a playlist's song
     * @throws IOException Whenever the new image file cannot be set
     */
    public void changeImage() throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Image Files (*.jpg, *.png, *.bmp, *.gif)",
                        "*.jpg", "*.png", "*.bmp", "*.gif")
        );
        File newImageFile = fileChooser.showOpenDialog(getScene().getWindow());
        if(newImageFile == null)
            return;
        ObservableList<Song> selectedSongs = getListView().getSelectionModel().getSelectedItems();
        String changeImageOption = "Change Image";
        if(selectedSongs.size() > 1)
            changeImageOption += "s";
        fileChooser.setTitle(changeImageOption);
        for(Song song : selectedSongs){
            File oldImageFile = song.getImageFile();
            if(oldImageFile != null){
                song.deleteImageFile(selectedSongs, oldImageFile);
            }
            song.setImageFile(newImageFile);
        }
        getListView().refresh();
    }

    /**
     * Deletes a song from this playlist's list of songs
     */
    public void removeSong(){
        ObservableList<Song> songsSelected = getListView().getSelectionModel().getSelectedItems();
        home.playlistListView.getSelectionModel().getSelectedItem().getSongs().removeAll(songsSelected);
        getListView().getItems().removeAll(songsSelected);
        home.updateLists();
    }

    /**
     * Starts editing of a genre table cell
     */
    @Override
    public void startEdit() {
        if(!isEditable())
            return;
        setStyle("-fx-cell-size: 59");
        super.startEdit();
        setText(null);
        setGraphic(editNameTextField);
        editNameTextField.setText(getItem().getName());
        editNameTextField.requestFocus();
        editNameTextField.focusedProperty().addListener((observable, oldVal, newVal) -> {
            if(!newVal)
                cancelEdit();
        });
        editNameTextField.selectAll();
    }

    /**
     * Commits the edit by accessing the genre text field's text and setting the new song genre with its value
     * @param newValue New value to commit
     */
    @Override
    public void commitEdit(Song newValue) {
        newValue.setName(editNameTextField.getText());
        super.commitEdit(newValue);
        setEditable(false);
        setStyle(null);
    }

    /**
     * Wraps up, or cancels an edit
     */
    @Override
    public void cancelEdit() {
        super.cancelEdit();
        updateItem(getItem(), isEmpty());
        setStyle(null);
        setEditable(false);
    }

    /**
     * Defines layout of a PlaylistSongCell
     * @param item Item to be updated
     * @param empty Denotes whether the cell is empty or not
     */
    @Override
    protected void updateItem(Song item, boolean empty) {
        super.updateItem(item, empty);
        if(empty || item == null){
            setText(null);
            setGraphic(null);
        } else{
            getStyleClass().add("standard_label");
            setOnMouseClicked(event -> {
                if(event.getClickCount() >= 2)
                    Home.handlePlayRequest(getItem(), home.playlistListView.getSelectionModel().getSelectedItem().getSongs());
                updateContextMenu();
            });
            setOnKeyPressed(event -> {
                if(event.getCode() == KeyCode.ENTER)
                    commitEdit(getItem());
            });
            getStyleClass().add("playlist_cell");
            setText(item.toString());
            if(item.getImage() != null) {
                setGraphicTextGap(15);
                ImageView image = new ImageView(item.getImage());
                image.setFitWidth(100);
                image.setFitHeight(80);
                image.setPreserveRatio(false);
                image.setSmooth(true);
                setGraphic(image);
            }
            else{
                setGraphicTextGap(40);
                FontAwesomeIconView defaultIcon = new FontAwesomeIconView();
                defaultIcon.setGlyphName("MUSIC");
                defaultIcon.setSize("85");
                setGraphic(defaultIcon);
            }
        }
    }
}
