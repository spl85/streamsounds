package Controller.Cells;

import Controller.Home;
import Model.Song;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.FileChooser;
import java.io.File;
import java.io.IOException;

/** A class that further defines the layout of a list of songs belonging to the home's song table */
public class SongNameTableCell extends TableCell<Song, String> {

    /** Pointer to home controller in order to update user's fields when updates are made */
    Home home;

    /** Text field to input the playlist's song's new name */
    TextField editNameField;

    /**
     * Returns instances of this cell
     * @param home Pointer to the home instance which owns this song cell
     */
    public SongNameTableCell(Home home){
        setEditable(false);
        this.home = home;
        editNameField = new TextField();
        editNameField.getStyleClass().add("standard-text-field");
    }

    /**
     * Updates the context menu when the user clicks on the cell, and sets up a potential edit
     */
    public void updateContextMenu(){
        MultipleSelectionModel model = getTableView().getSelectionModel();
        boolean multipleSelected = false;
        if(model.getSelectedIndices().size() > 1)
            multipleSelected = true;
        ContextMenu menu = new ContextMenu();
        MenuItem play = new MenuItem("Play");
        play.setOnAction(event -> Home.handlePlayRequest((Song)getTableRow().getItem(), home.getSongLibrary()));
        FontAwesomeIconView playIcon = new FontAwesomeIconView();
        playIcon.setGlyphName("PLAY");
        play.setGraphic(playIcon);
        MenuItem renameSong = new MenuItem("Rename Song");
        renameSong.setOnAction(event -> {
            setEditable(true);
            startEdit();
        });
        FontAwesomeIconView renameSongIcon = new FontAwesomeIconView();
        renameSongIcon.setGlyphName("STRIKETHROUGH");
        renameSong.setGraphic(renameSongIcon);
        String changeImageOption = "Change Image";
        if(multipleSelected)
            changeImageOption += "s";
        MenuItem changeImage = new MenuItem(changeImageOption);
        changeImage.setOnAction(event -> {
            try {
                changeImage();
            } catch (IOException e) {
            }
        });
        FontAwesomeIconView changeImageIcon = new FontAwesomeIconView();
        changeImageIcon.setGlyphName("EXCHANGE");
        changeImage.setGraphic(changeImageIcon);
        String deleteOption = "Delete Song";
        if(multipleSelected)
            deleteOption += "s";
        MenuItem delete = new MenuItem(deleteOption);
        delete.setOnAction(event -> deleteSong());
        FontAwesomeIconView deleteIcon = new FontAwesomeIconView();
        deleteIcon.setGlyphName("TRASH");
        delete.setGraphic(deleteIcon);
        if(!multipleSelected)
            menu.getItems().addAll(play, renameSong, changeImage, delete);
        else
            menu.getItems().addAll(changeImage, delete);
        setContextMenu(menu);
    }

    /**
     * Starts editing of a song name table cell
     */
    @Override
    public void startEdit() {
        if(!isEditable())
            return;
        getTableRow().setStyle("-fx-cell-size: 67");
        getTableRow().startEdit();
        setText(null);
        setGraphic(editNameField);
        setAlignment(Pos.CENTER_LEFT);
        Song song = (Song)getTableRow().getItem();
        editNameField.setText(song.getName());
        editNameField.requestFocus();
        editNameField.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!newValue)
                cancelEdit();
        });
        editNameField.selectAll();
    }

    /**
     * Commits the edit by accessing the song name text field's text and setting the new song name with its value
     * @param newValue New value to commit
     */
    @Override
    public void commitEdit(String newValue) {
        newValue = editNameField.getText();
        Song song = (Song)getTableRow().getItem();
        song.setName(newValue);
        getTableRow().commitEdit(getTableRow().getItem());
        setEditable(false);
        getTableRow().setStyle(null);
        cancelEdit();
    }

    /**
     * Wraps up, or cancels an edit
     */
    @Override
    public void cancelEdit() {
        getTableRow().cancelEdit();
        updateItem(getItem(), isEmpty());
        setStyle(null);
        getTableRow().setStyle(null);
        setEditable(false);
    }

    /**
     * Changes image of a song
     * @throws IOException Whenever the new image file cannot be set
     */
    public void changeImage() throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Image Files (*.jpg, *.png, *.bmp, *.gif)",
                        "*.jpg", "*.png", "*.bmp", "*.gif")
        );
        String changeImageOption = "Change Image";
        if(getTableView().getSelectionModel().getSelectedItems().size() > 1)
            changeImageOption += "s";
        fileChooser.setTitle(changeImageOption);
        File newImageFile = fileChooser.showOpenDialog(getScene().getWindow());
        if(newImageFile == null)
            return;
        ObservableList<Song> selectedSongs = getTableView().getSelectionModel().getSelectedItems();
        for(Song song : selectedSongs){
            File oldImageFile = song.getImageFile();
            song.deleteImageFile(selectedSongs, oldImageFile);
            song.setImageFile(newImageFile);
        }
        getTableView().refresh();
    }

    /**
     * Deletes a song from the user's list of songs (this will make the song object susceptible to garbage collection)
     */
    public void deleteSong(){
        ObservableList<Song> selectedSongs = getTableView().getSelectionModel().getSelectedItems();
        selectedSongs
                .stream()
                .forEach(selectedSong -> {
                    selectedSong.deleteImageFile(selectedSongs, selectedSong.getImageFile());
                    selectedSong.deleteAudioFile(selectedSongs, selectedSong.getAudioFile());
                });
        home.getPlaylists()
                .stream()
                .forEach(playlist -> playlist.getSongs().removeAll(selectedSongs));
        getTableView().getItems().removeAll(selectedSongs);
        getTableView().refresh();
        home.updateLists();
    }

    /**
     * Defines layout of a SongNameTableCell
     * @param item Item to be updated
     * @param empty Denotes whether the cell is empty or not
     */
    @Override
    protected void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);
        if(empty || item == null){
            setOnMouseClicked(null);
            setOnKeyPressed(null);
            setEditable(false);
            setOnMouseClicked(null);
            setOnKeyPressed(null);
            setGraphic(null);
            setText(null);
        } else{
            setOnMouseClicked(event -> {
                if(event.getClickCount() >= 2)
                    Home.handlePlayRequest((Song)getTableRow().getItem(), home.getSongLibrary());
                updateContextMenu();
            });
            setOnKeyPressed(event -> {
                KeyEvent keyEvent = event;
                if(keyEvent.getCode() == KeyCode.ENTER)
                    commitEdit(editNameField.getText());
            });
            Song song = (Song)getTableRow().getItem();
            if(song == null)
                return;
            setText(song.getName());
            if(song.getImage() != null){
                setGraphicTextGap(6);
                ImageView imageView = new ImageView(song.getImage());
                imageView.setFitWidth(70);
                imageView.setFitHeight(60);
                imageView.setPreserveRatio(false);
                imageView.setSmooth(true);
                setGraphic(imageView);
            } else{
                setGraphicTextGap(26);
                FontAwesomeIconView icon = new FontAwesomeIconView();
                icon.setGlyphName("MUSIC");
                icon.setSize("60");
                setGraphic(icon);
            }
        }
    }
}
