package Controller.Cells;

import Model.Song;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.geometry.Pos;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableCell;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;

/** Class that defines a composer cell */
public class ComposerTableCell extends TableCell<Song, String> {

    /** Text field to input composer's name */
    TextField editComposerTextField;

    /**
     * Constructs a ComposerTableCell object
     */
    public ComposerTableCell(){
        setEditable(false);
        setAlignment(Pos.CENTER_LEFT);
        editComposerTextField = new TextField();
        editComposerTextField.getStyleClass().add("standard-text-field");
    }

    /**
     * Updates the context menu when the user clicks on the cell, and sets up a potential edit
     */
    public void updateContextMenu(){
        boolean multipleSelected = false;
        if(getTableView().getSelectionModel().getSelectedIndices().size() > 1)
            multipleSelected = true;
        ContextMenu menu = new ContextMenu();
        String changeComposerOption = "Change Composer";
        if(multipleSelected)
            changeComposerOption += "s";
        MenuItem changeComposer = new MenuItem(changeComposerOption);
        changeComposer.setOnAction(event -> {
            setEditable(true);
            startEdit();
        });
        FontAwesomeIconView changeComposerIcon = new FontAwesomeIconView();
        changeComposerIcon.setGlyphName("STRIKETHROUGH");
        changeComposer.setGraphic(changeComposerIcon);
        menu.getItems().add(changeComposer);
        setContextMenu(menu);
    }

    /**
     * Starts editing of a composer table cell
     */
    @Override
    public void startEdit() {
        if(!isEditable())
            return;
        getTableRow().setStyle("-fx-cell-size: 67");
        if(getItem() == null)
            return;
        getTableRow().startEdit();
        setText(null);
        setGraphic(editComposerTextField);
        setAlignment(Pos.CENTER_LEFT);
        editComposerTextField.setText(getItem());
        editComposerTextField.requestFocus();
        editComposerTextField.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if(!newValue)
                cancelEdit();
        });
        editComposerTextField.selectAll();
    }

    /**
     * Commits the edit by accessing the composer text field's text and setting the new song composer with its value
     * @param newValue New value to commit
     */
    @Override
    public void commitEdit(String newValue) {
        newValue = editComposerTextField.getText();
        for(Song song : getTableView().getSelectionModel().getSelectedItems())
            song.setComposer(newValue);
        setEditable(false);
        getTableRow().setStyle(null);
        getTableView().refresh();
        cancelEdit();
    }

    /**
     * Wraps up, or cancels an edit
     */
    @Override
    public void cancelEdit() {
        getTableRow().cancelEdit();
        updateItem(getItem(), isEmpty());
        setStyle(null);
        getTableRow().setStyle(null);
        setEditable(false);
    }

    /**
     * Updates the item that belongs to this table cell
     * @param item Item to update
     * @param empty True if the cell is empty, false otherwise
     */
    @Override
    protected void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);
        if(empty || item == null){
            setGraphic(null);
            setText(null);
        } else{
            setOnMouseClicked(event -> updateContextMenu());
            setOnKeyPressed(event -> {
                if(event.getCode() == KeyCode.ENTER)
                    commitEdit(editComposerTextField.getText());
            });
            Song song = (Song)getTableRow().getItem();
            if(song == null)
                return;
            setGraphic(null);
            setText(song.getComposer());
        }
    }
}
