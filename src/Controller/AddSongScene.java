package Controller;

import Model.Song;
import Model.User;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import java.io.File;
import java.io.IOException;
import static javafx.scene.paint.Color.GOLDENROD;

/**
 * Class for handling events on add song window
 */
public class AddSongScene {

    /** FXML Injectables */
    @FXML
    Button audioFileButton;
    @FXML
    TextField composerTextField;
    @FXML
    TextField genreTextField;
    @FXML
    FontAwesomeIconView checkIcon;
    @FXML
    Label imageFilePathLabel;
    @FXML
    Button imageFileButton;
    @FXML
    Button folderButton;
    @FXML
    FontAwesomeIconView defaultIcon;
    @FXML
    Label songNameLabel;
    @FXML
    ImageView songImageView;
    @FXML
    Label audioFilePathLabel;
    @FXML
    Label pathToImageFileLabel;
    @FXML
    FontAwesomeIconView addSongsCheckIcon;
    @FXML
    Label musicDirectoryLabel;

    /** Pointer to SongAddable interface so that it can specify how to add a song to its particular scene */
    SongAddable scene;

    /** Stage that this scene belongs to */
    Stage thisStage;

    /** Pointer to the final song the user wishes to add */
    Song newSong;

    /** Denotes whether or not an image file has been set on a song */
    public boolean imageFileSet;

    /**
     * Initializes this scene with necessary fields
     * @param thisStage Stage that this scene belongs to
     * @param scene Pointer to SongAddable interface so that it can specify how to add a song to its particular scene
     */
    public void start(Stage thisStage, SongAddable scene){
        newSong = new Song();
        imageFileSet = false;
        this.scene = scene;
        this.thisStage = thisStage;
        FontAwesomeIconView folderIcon = new FontAwesomeIconView();
        folderIcon.setGlyphName("FOLDER_OPEN");
        folderIcon.setSize("14");
        folderIcon.setFill(GOLDENROD);
        audioFileButton.setGraphic(folderIcon);
        FontAwesomeIconView imageFileIcon = new FontAwesomeIconView();
        imageFileIcon.setGlyphName("FOLDER_OPEN");
        imageFileIcon.setSize("14");
        imageFileIcon.setFill(GOLDENROD);
        imageFileButton.setGraphic(imageFileIcon);
        FontAwesomeIconView folderButtonIcon = new FontAwesomeIconView();
        folderButtonIcon.setGlyphName("FOLDER_OPEN");
        folderButtonIcon.setSize("14");
        folderButtonIcon.setFill(GOLDENROD);
        folderButton.setGraphic(folderButtonIcon);
        if(scene instanceof PlaylistScene){
            PlaylistScene ps = (PlaylistScene)scene;
            if(ps.playlist.getImage() != null){
                defaultIcon.setVisible(false);
                songImageView.setVisible(true);
                songImageView.setImage(ps.playlistImage.getImage());
                imageFilePathLabel.setText(ps.playlist.getImageFile().getPath());
            }
        }
    }

    /**
     * Handles creation of a new song
     * @throws IOException Whenever song's image/audio files cannot be set
     */
    public void createSong() throws IOException {
        /* get image file */
        File imageFile = new File(imageFilePathLabel.getText());
        if(!imageFile.exists()) {
            newSong.setImageFile(null);
        }
        else {
            newSong.setImageFile(imageFile);
        }
        /* get audio file */
        File audioFile = new File(audioFilePathLabel.getText());
        if(!audioFile.exists()){
            /* display error */
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Cannot Create Song");
            alert.setHeaderText("Audio file provided does not exist");
            alert.setContentText("Try setting the audio file again.");
            alert.showAndWait();
            return;
        }
        newSong.setAudioFile(audioFile);
        scene.addSong(newSong);
        thisStage.close();
    }

    /**
     * Updates genre of a song whenever user types information
     */
    public void updateGenre(){
        newSong.setGenre(genreTextField.getText());
        songNameLabel.setText(newSong.toStringFull());
    }

    /**
     * Updates composer of a song whenever user types information
     */
    public void updateComposer(){
        newSong.setComposer(composerTextField.getText());
        songNameLabel.setText(newSong.toStringFull());
    }

    /**
     * Handles creation of a song
     * @throws IOException Whenever song's audio file cannot be set
     */
    public void createSongs() throws IOException {
        File musicDir = new File(musicDirectoryLabel.getText());
        for(File file : musicDir.listFiles()){
            if(file.isDirectory())
                continue;
            if(!(User.getFileExtension(file).equals("mp3")
                    || User.getFileExtension(file).equals("wav")
                    || User.getFileExtension(file).equals("aac")
                    || User.getFileExtension(file).equals("aiff")))
                continue;
            Song song = new Song();
            song.setName(file.getName().substring(0, file.getName().lastIndexOf('.')));
            song.setAudioFile(file);
            if(scene instanceof PlaylistScene)
                song.setImageFile(((PlaylistScene)scene).playlist.getImageFile());
            scene.addSong(song);
        }
        thisStage.close();
    }

    /**
     * Adds an image to a song, and updates the view with the user's selection
     */
    public void addImage() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Image File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Image Files (*.png, *.jpg, *.gif)", "*.png", "*.jpg", "*.gif")
        );
        File imageFile = fileChooser.showOpenDialog(thisStage);
        if(imageFile != null) {
            imageFileSet = true;
            imageFilePathLabel.setText(imageFile.getPath());
            songImageView.setVisible(true);
            songImageView.setImage(new Image(imageFile.toURI().toString()));
            defaultIcon.setVisible(false);
        } else{
            imageFileSet = false;
            if(scene instanceof PlaylistScene) {
                PlaylistScene ps = (PlaylistScene)scene;
                if(ps.playlist.getImage() == null) {
                    defaultIcon.setVisible(true);
                    songImageView.setVisible(false);
                    imageFilePathLabel.setText("N/A");
                } else{
                    songImageView.setImage(ps.playlistImage.getImage());
                    songImageView.setVisible(true);
                    defaultIcon.setVisible(false);
                    imageFilePathLabel.setText(ps.playlist.getImageFile().getPath());
                }
            }
            else {
                defaultIcon.setVisible(true);
                songImageView.setVisible(false);
            }
        }
    }

    /**
     * Adds audio file to a song
     */
    public void addAudio() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Audio File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Audio Files (*.wav, *.mp3, *.aac, *.aiff)",
                        "*.wav", "*.mp3", "*.aac", "*.aiff")
        );
        File audioFile = fileChooser.showOpenDialog(thisStage);
        if(audioFile != null) {
            checkIcon.setVisible(true);
            audioFilePathLabel.setText(audioFile.getPath());
            newSong.setName(audioFile.getName().substring(0, audioFile.getName().lastIndexOf('.')));
            songNameLabel.setText(newSong.toStringFull());
        }
        else {
            checkIcon.setVisible(false);
            audioFilePathLabel.setText("N/A");
            newSong.setName("Song");
            songNameLabel.setText(newSong.toStringFull());
        }
    }

    /**
     * Handles multiple songs folder button click
     */
    public void addSongs() {
        DirectoryChooser dc = new DirectoryChooser();
        dc.setTitle("Directory Containing Music Files");
        File musicDir = dc.showDialog(thisStage);
        if(musicDir == null) {
            musicDirectoryLabel.setText("N/A");
            addSongsCheckIcon.setVisible(false);
        } else{
            musicDirectoryLabel.setText(musicDir.getPath());
            addSongsCheckIcon.setVisible(true);
        }
    }

}
