package Controller;

import Model.Song;

/**
 * A scene that can specify in which way to update it's FXML fields
 * so that the added song is reflected in the respective scene's FXML observable collections
 * that it is displaying to the user
 */
@FunctionalInterface
interface SongAddable {

    /**
     * Adds a song to the respective scene, and carries out the necessary process to update its FXML fields, and observable collections
     * @param song Song to add
     */
    void addSong(Song song);
}
